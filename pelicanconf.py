##!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from markdown import Markdown
import re

### METADATA
##########

AUTHOR = u'Frank Foerster'
SITENAME = u'Frank Foerster'
SITETITLE = u'Developmental Robotics and Human-Robot Interaction'
BANNER_SUBTITLE= u'Developmental Robotics and Human-Robot Interaction'

TIMEZONE = 'Europe/London'
DEFAULT_LANG = 'en'

### PATH SETTINGS (relative)
##########

# pages and articles
PATH = 'content'
ARTICLE_PATHS = [ 'blog' ]

# templates (in addition to theme templates) and plugins
PLUGIN_PATHS = [ 'pelican-plugins', 'plugins' ]

# things to copy
STATIC_PATHS = [
  'images',
  'extra',
  'publications'
]

# Tell Pelican to change the path to 'static/custom.css' in the output dir
EXTRA_PATH_METADATA = {
  'extra/custom.css': {'path': 'static/css/custom.css'},
  'extra/custom.js': {'path': 'static/js/custom.js'},
  'extra/cookies-eu-banner.min.js': {'path': 'static/js/cookies-eu-banner.min.js'},
  'extra/robots.txt': {'path': 'robots.txt'}
}

CUSTOM_CSS = 'static/css/custom.css'
CUSTOM_JS = 'static/js/custom.js'

# Supress generation of blogging-related sites
ARCHIVES_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
TAGS_SAVE_AS = ''
INDEX_SAVE_AS = ''

### DISPLAY OPTIONS
##########

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Twitter button
TWITTER_USERNAME = "FrankPFoerster"
TWITTER_WIDGET_ID = "1"

# Blogroll
LINKS = [
    ('Robotics research group', 'http://robotics.herts.ac.uk'),
    ('Adaptive Systems Research Group', 'http://adapsys.cs.herts.ac.uk/'),
    ('iCub Robot', 'https://icub.iit.it/')
]

# Social widget
SOCIAL = [
  ('Scholar', 'https://scholar.google.com/citations?user=7BWvjPYAAAAJ', 'user'),
  ('ORCiD', 'https://orcid.org/0000-0003-1797-682X', 'user'),
  ('Twitter', 'https://twitter.com/FrankPFoerster'),
  ('E-mail', 'mailto:&#102;&#046;&#102;&#111;&#101;&#114;&#115;&#116;&#101;&#114;&#064;&#104;&#101;&#114;&#116;&#115;&#046;&#097;&#099;&#046;&#117;&#107;', 'envelope')
]

# Menu items
DISPLAY_PAGES_ON_MENU = True
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
# make article page a separate "blogs" page (our index is a static page)

#MENUITEMS = [
  #('Blog', '/blog/')
  #('Curriculum Vitae', '/curriculum-vitae/'),
  #('Publications', '/publications/')
#]

### THEME SETTINGS
##########
THEME = 'pelican-themes/pelican-bootstrap3'
BOOTSTRAP_THEME = 'simplex'
THEME_TEMPLATES_OVERRIDES = [ 'templates' ]
I18N_TEMPLATES_LANG = 'en'
#FAVICON = ''
FAVICON = 'images/signal-agents512.webp'
#SITELOGO = ''
#SITELOGO_SIZE = ''
AVATAR = 'images/avatar.webp'
#ABOUT_ME = 'I am <a href="https://www.herts.ac.uk/study/schools-of-study/engineering-and-computer-science/research-in-engineering-and-computer-science/the-phd-programme-in-computer-science">advertising</a> the doctoral project<br><strong><a href="https://www.herts.ac.uk/__data/assets/pdf_file/0004/284359/Social-Credibility-of-Interactive-Robots.pdf">Social Credibility of Interactive Robots</a>!</strong>'

#RECENT_NEWS_HEADER = ''
RECENT_NEWS = ''

# 'Workshop:\
# <ul>\
#   <li>\
#     <b>WTF Workshop:</b><br>\
#     <a href="https://sites.google.com/view/wtfworkshop2022/overview">Working with Troubles and Failures in Conversation Between Humans and Robots</a><br>\
#     Deadline: 22 Jul 2022<br>\
#   </li>\
# </ul>'

HIDE_SITENAME = False
DISPLAY_ARTICLE_INFO_ON_INDEX = False
BANNER = 'images/icub_compound.webp'
BANNER_ALL_PAGES = True
RESPONSIVE_IMAGES = True
JINJA_ENVIRONMENT = { 'extensions': ['jinja2.ext.i18n'] }
#DEFAULT_PAGINATION = False
# i18n needed for bootstrap3 theme
PLUGINS = [ 'i18n_subsites' ]
#SIDEBAR_IMAGES_HEADER = ''
#SIDEBAR_IMAGES = ["",""]

### PLUGIN SETTINGS
PLUGINS.append('cv')

# sitemap
PLUGINS.append('sitemap')
SITEMAP= {'format': 'xml',
          'exclude': 'about/'}

# bibtex files
PLUGINS.append('pelican-bibtex')
PUBLICATIONS_NAVBAR = True
PUBLICATIONS = {
  'a-peer': {
    'title': 'Peer Reviewed',
    'file': 'peer.bib',
    'split_link': False,
    'highlight': ['Frank Förster'],
    'group_type': True},
  'b-others': {
    'title': 'Others',
    'file': 'others.bib',
    'split': False,
    'bottom_link': False,
    'highlight': ['Frank Förster'] }
}
# markdown processor for jinja
markdown = Markdown(extensions=['markdown.extensions.extra'])
def md(content, *args):
    p = '<p>'
    np = '</p>'
    md = markdown.convert(content)
    if md.startswith(p) and md.endswith(np): #you filthy bastard
      md = md[len(p):-len(np)]
    return md

def clean(content, *args):
    return re.sub('\W+', '', content)

JINJA_FILTERS = {
    'md': md,
    'clean' : clean
}


CV_CONTENTS = 'content/cv.yaml'
CV_PDF = False
CV_PDF_TEMPLATE = 'cv.tex'
CV_PDF_RESULT = 'curriculum-vitae/cv.pdf'
CV_ADDITIONAL_SOURCES = [ 'peer.bib', 'others.bib', 'letter.tex', 'plainyrrev.bst' ]
CV_PREAMBLE = 'letter.tex'
CV_NAVBAR = True
CV_METRICS = True
