---
title: About
subtitle: Frank Förster
save_as: index.html
documentclass: scrartcl
header-includes: \usepackage{noto}
status: hidden
template: about
---


I am a Senior Lecturer at the [University of Hertfordshire](http://www.herts.ac.uk/) (UK), member of the [Robotics Research Group](https://robotics.herts.ac.uk/), and affiliated with the [Adaptive Systems Research Group](http://adapsys.cs.herts.ac.uk/).

##Teaching
My teaching focusses mostly on Artificial Intelligence and Robotics. On the BSc level I teach and lead *Introduction to Programming and Discrete Structures* (4WCM2006), and *Introduction to Robotics* (4ENT1177). On the MSc level I teach and lead the course *Theory and Practice of Artificial Intelligence* (7WCM0061). I am also involved in teaching *AI Robotics and Applications* (6COM1036) and *Team Research and Development* (7COM1079). In the past, I have taught *Models and Methods of Computing* (4WCM0021), *Research Methods* (7COM1085) in the the summer semester (semester C). Beyond that, I also act as supervisor for a good number of BSc project, primarily those that contain an AI element.

##Research
My research is mostly located at the intersection of developmental / cognitive robotics and human-robot interaction.
Research interests include (robotic) language acquisition in particular and communicative interaction between human and machine in general and my research is generally heavily influenced by cognitive science, developmental psychology, and conversation analysis. I have worked on motor resonance between human-robot dyads in the past.
The underlying assumption behind much of my research is that regularities in human social interaction are one of the least understood and most underrated phenomena when it comes to creating artificial intelligence for robots if these are ought to interact with humans. 

####Ongoing Projects
I am currently involved in the **[FLUIDITY](https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/X009343/1)** project, led by Julian Hough at Swansea Univeristy, where we are trying to create more natural and fluid human-robot communication, both in simulation as with physical robots.

####Workshops
I have recently organised the 2nd iteration of the **[WTF workshop](https://sites.google.com/view/wtf2023/overview)** ("Working with Trouble and Failures in conversation between humans and robots") at **[CUI'23](https://cui.acm.org/2023/)**. The workshop was an international follow-up to the first, UK-based [WTF workshop](https://sites.google.com/view/wtfworkshop2022/overview) on failures and miscommunication in robotic speech interfaces in 2022 sponsored by the UK-RAS network. Before that I organised the [ICRA'20](https://handovers.gitlab.io/icra2020-handovers/) workshop on human-robot handovers.

###Editorial Work
I act as associate editor of the [IEEE Transactions on Cognitive and Developmental Systems](https://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=7274989), and am active reviewer for other robotics and AI conferences and journals.

##Past Work and Projects
Before joining the University of Hertfordshire, I was a postdoctoral research assistant (PDRA) at [Advanced Robotics @ Queen Mary](https://www.robotics.qmul.ac.uk/) in the [Turing project on intuitive human-robot interaction in work environments](https://www.turing.ac.uk/research/research-projects/intuitive-human-robot-interaction-work-environments). Prior to this I organised and ran experiments on motor resonance within the [HRI-BioPsy project](http://hri-biopsy.herts.ac.uk/) at the [Adaptive Systems Research Group](http://adapsys.cs.herts.ac.uk/). Earlier, I worked as software integrator in the [REVERIE project](https://cordis.europa.eu/project/id/287723) at the [Multimedia and Vision Group at Queen Mary](http://mmv.eecs.qmul.ac.uk/).

In 2013, I received my Ph.D. on the topic of [Negation in Robotic Language Acquisition](https://uhra.herts.ac.uk/handle/2299/20781). Much of this work was performed within the [EU FP7 ITALK project](https://italkproject.org/).
Prior to the PhD, I obtained a BSc and MSc at Ulm University, where I specialised on artificial neural networks and worked as tutor for courses _Neuroinformatik I_ (~_Introduction to Neural Networks_) and _Neuroinformatik II_ (~_Theory of Neural Networks_ ). My MSc thesis was on _Ensemble Methods to Combine Uncertain Classifiers_ supervised by [Prof. Friedhelm Schwenker](https://www.uni-ulm.de/in/neuroinformatik/institut/hidden/f-schwenker/).
While studying computer science, I also studied philosophy on the side - without degree - at the [Humboldt Centre](https://www.uni-ulm.de/en/einrichtungen/humboldt-studienzentrum-fuer-philosophie-und-geisteswissenschaften/) of Ulm University, mostly focusing on philosophy of language, Wittgenstein and Searle. My studies of Wittgenstein ultimately led me to my PhD research topic and robotics more generally.
