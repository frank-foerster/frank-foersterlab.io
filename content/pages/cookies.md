---
title: Cookie Usage
status: hidden
---

### Essential cookies

<div class="form-check">
  <input type="checkbox" class="form-check-input" id="cookies-necessary-state" data-toggle='toggle' checked="true" disabled="true">
  <label class="form-check-label" id="cookies-necessary-text" for="cookies-necessary-state">Enabled (necessary)</label>
</div>

My site uses a single cookie to manage your third party cookies settings. This cookie cannot be disabled.

### Third party cookies

<div class="form-check">
    <input type="checkbox" class="form-check-input" id="cookies-eu-state" data-toggle='toggle' checked="true">
    <label class="form-check-label" id="cookies-eu-text" for="cookies-eu-state">Enabled</label>
  </div>

If third party cookies are enabled, the website uses Google Analytics for traffic analysis including the pages you visit, the type and language settings of the browser you use, your approximate location, and your operating system. I have also embedded my own Twitter feed where you might be tracked if you interact with the widget.

Read how these companies handle personal data:

 * [Google Analytics](https://support.google.com/analytics/topic/2919631?hl=en)
 * [Twitter Widgets](https://developer.twitter.com/en/docs/twitter-for-websites/privacy)
